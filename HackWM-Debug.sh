#!/bin/bash

# HackWM test script
# Runs HackWM in it's own seperate window to avoid conflicts
# by Kat Hamer

OLDDISPLAY=$DISPLAY  # Existing display
USEDISPLAY=":1"  # Display to run X server on
HOSTNAME=$(hostname)  # Current hostname
FILE="HackWM.py"

killall Xephyr  &>/dev/null # Kill any existing Xephyr windows silently
export DISPLAY=:0  # Set display to main display
Xephyr -screen $1 -br :1 &  # Start new Xephyr session
export DISPLAY=:1  # Set display to Xephyr window
echo "HackWM is running on $HOSTNAME$USEDISPLAY... (Press CTRL+C to stop)"
echo  # Newline
python3  $FILE   # Run HackWM and block further execution
pkill Xephyr  &>/dev/null # Kill Xephyr session silently
export DISPLAY=$OLDDISPLAY  # Revert display back to existing display
echo  # Newline

echo "HackWM stopped..."
