"""
HackWM Wrapper
Window manager loop and main module
By Kat Hamer
"""

import Core as HackWM
from time import sleep
from Modules import log

def loop(hackwm):
    """Basic window manager loop"""
    hackwm.handleEvents()  # Handle window manager events
    hackwm.focusedWindow = hackwm.queryFocused()  # Get currently focused window
    if hackwm.focusedWindow:  # If the currently focused window is not the root window
        hackwm.stealFocus(hackwm.focusedWindow)  # Focus window
    for window in hackwm.windowList:
        hackwm.borderHandler.draw(window)  # Draw a border on each window
        #print(window.get_attributes())
    hackwm.doHook.loop()
    sleep(hackwm.processorIdleTime)

def main():
    hackwm = HackWM.hackwm()
    hackwm.startup()
    while True:
        try:
            loop(hackwm)
        except KeyboardInterrupt:
            print("Interrupt!")
            hackwm.closeDisplay()
            exit()
        except error.ConnectionClosedError:
            log.log("server", "X server closed")
            hackwm.closeDisplay()

if __name__ == "__main__":
    print("...")
    main()
