"""
Config
Parse JSON config files
By Kat Hamer
"""

import json  # Load json files as dictionaries

defaultPath = "Config/WindowManager.conf"  # Default config path, needs to be hardcoded since we can't store it in a config file


def loadConfig(filePath=defaultPath):
    """Load a JSON config file as a config dictionary"""
    try:
        with open(filePath) as fileBuffer:
            jsonData = fileBuffer.read()  # Read the contents of the file
        config = json.loads(jsonData)  # Turn the file contents into a dictionary
    except IOError:  # Placeholder, should be replaced with proper logging
        print("Could not open config file!")
        exit()
    except json.decoder.JSONDecodeError as error:  # If there is a syntax error in the file
        print("Error in config file!")
        print(error)
        exit()
    return config
