"""
Command
Interpret config commands
By Kat Hamer
"""

from Modules import log  # Log function
import subprocess  # Launch processes
import os  # Get environment variables

def toggleHidden(config, wm):
    """Toggle hidden window"""
    wm.toggleHidden()

def fullscreen(config, wm):
    """Make a window fullscreen"""
    wm.fullscreen()

def lockWindow(config, wm):
    """Lock a window"""
    wm.lockWindow()


def launchBrowser(config, wm):
    """Launch a browser"""
    browser = config["applications"]["browser"]  # Get preferred browser from config
    log.log("info", "launching browser ({})".format(browser))
    subprocess.Popen(browser)  # Spawn a browser


def launchTerm(config, wm):
    """Launch a terminal"""
    term = config["applications"]["terminal"]  # Get path of terminal to launch
    log.log("info", "launching a terminal ({})".format(term))
    homeDirectory = os.environ["HOME"]   # Get home directory to spawn terminal in
    subprocess.Popen(term, cwd=homeDirectory, shell=True)  # Spawn the terminal


def destroyWindow(config, wm):
    """Destroy currently focused window"""
    wm.destroyWindow()


def tilingHorizontal(config, wm):
    """Set tiling mode to horizontal"""
    wm.tilingMode = "horizontal"


def tilingVertical(config, wm):
    """Set tiling mode to vertical"""
    wm.tilingMode = "vertical"

def status(config, wm):
    print("Status: OK!")

commandDispatch = {"launch_term": launchTerm,
                   "launch_browser": launchBrowser,
                   "destroy_window": destroyWindow,
                   "lock_window": lockWindow,
                   "fullscreen": fullscreen,
                   "toggle_hidden": toggleHidden,
                   "status": status}


def interpret(command, config, wm):
    """Interpret a command and dispatch it to the correct function"""
    if command in commandDispatch:
        commandDispatch[command](config, wm)  # Run the command with config and wm as paramters
    else:
        log.log("error", "{} not a valid command!".format(command))
