"""
Workspace
Handle workspace switching
By Kat Hamer
"""

workspaces = {"1":[],
              "2":[],
              "3":[]}

def switchWorkspace(wm, workspaceNumber):
    for window in wm.currentWorkspace:
        window.unmap()
    wm.currentWorkspace = workspaces.keys()[workspaceNumber]  # Get workspace at index
    for window in wm.currentWorkspace:
        window.map

