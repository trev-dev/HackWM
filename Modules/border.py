"""
Border
Draw window borders
By Kat Hamer
"""


class border(object):
    """Main border class"""

    def __init__(self, config, wm):
        """Init function"""
        self.wm = wm  # Make window manager class global
        self.colormap = wm.display.screen().default_colormap  # Load colormap from display
        self.activeColor = config["border"]["activeColor"]  # Color to display when a window is focused
        self.inactiveColor = config["border"]["inactiveColor"]  # Color to display when a window is not focused
        self.borderWidth = config["border"]["width"]  # Width of border in pixels

    def draw(self, window):
        """Draw the border"""
        self.focusedWindow = self.wm.focusedWindow

        if window == self.focusedWindow:  # If the window is focused
            borderColor = self.activeColor  # Use the active border color
        else:
            borderColor = self.inactiveColor  # Otherwise use the inactive border color
        pixelColor = self.colormap.alloc_named_color(borderColor).pixel  # Get color from colormap
        window.configure(border_width=self.borderWidth)  # Set border width of window
        window.change_attributes(None, border_pixel=pixelColor)  # Set border color of window
        self.wm.display.sync()  # Sync the display so changes can be seen
