"""
Keyboard
Keyboard handler for window managers
by Kat Hamer
"""

"""Xlib imports for interfacing with keyboard"""
from Xlib import X, XK

"""wm imports"""
from Modules import log  # Logging function
from Modules import command  # Run commands from config file


class keyboard(object):

    def __init__(self, config, wm):
        """Init function: load configuration and key bindings"""
        self.config = config
        self.keyboardConfig = config["keyboard"]
        self.keyBindings = self.keyboardConfig["bindings"]  # Get all keybindings from config
        self.wm = wm
        self.keysyms = [item for item in dir(XK) if item[:3] == "XK_"]  # Get all available keysyms in the X library
        self.configureKeys()

    def getKey(self, key):
        """Convert a key name to a list of key codes"""
        keysym = self.convertToKeysym(key)  # Convert friendly key name into a keysym
        codes = self.getKeyCodes(keysym)  # Convert keysym into a set of codes that match the key
        return codes

    def convertToKeysym(self, key):
        """Convert a key name to a keysym"""
        for keysym in self.keysyms:  # Iterate through all avaliable keysyms
            if keysym[3:] == key:  # If the keysym is the same key as the friendly name
                key = getattr(XK, keysym)  # Return the value of the key
        return key

    def getKeyCodes(self, keysym):
        """Get a set of keycodes for a key"""
        codes = set(code for code, index in self.wm.display.keysym_to_keycodes(keysym))  # Generate a set of codes
        return codes

    def configureKeys(self):
        """Bind keys"""
        for key in self.keyBindings:  # Iterate through each keybinding
            modifier = getattr(X, self.keyboardConfig["modifier"])  # Get modifier from config
            key = self.convertToKeysym(key)  # Convert key name to a keysym
            codes = self.getKeyCodes(key)  # Get a set of codes that match the key
            for code in codes:  # Iterate through each available code
                self.wm.rootWindow.grab_key(code,  # Get events when that key is pressed
                                            modifier,
                                            1,
                                            X.GrabModeAsync,
                                            X.GrabModeAsync)

    def handleKeypress(self, event):
        """Handle key presses: Run whenever a key is pressed"""
        for key in self.keyBindings:  # Iterate through each keybinding
            if event.detail in self.getKey(key):  # If the event raised matches the key
                keyCommand = self.keyBindings[key]
                log.log("keypress", "{} was pressed.".format(key))  # Display a placeholder
                command.interpret(keyCommand, self.config, self.wm)

    def handleKeyrelease(self, event):
        for key in self.keyBindings:
            if event.detail in self.getKey(key):
                log.log("keyrelease", "{} was released.".format(key))
