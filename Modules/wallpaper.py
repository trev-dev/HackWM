"""
Wallpaper
Currently just calls Feh to provide a wallpaper but will soon be a standalone wallpaper manager
by Kat Hamer
"""

import subprocess  # Run a command

def setWallpaper(wallpaper):
    subprocess.Popen(["feh", "--bg-scale", wallpaper], stderr=subprocess.PIPE, stdout=subprocess.PIPE)

